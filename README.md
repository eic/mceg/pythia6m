# pythia6m


PYTHIA6 modified to better describe lepton-nucleon scattering at intermediate energies.
The modifications include a full simulation of radiative effects with RADGEN.

(based on the HERMES PYTHIA version)

NOTES:
- Normal lepto-production is fully tested, yields identical results to the original HERMES version
- Running with a bremsstrahlung beam is supported but untested
- The necessary front end to simulate radiative effects through RADGEN has not yet been implemented

# Installation
Note: the following instructions are given assuming you are using `bash` as your shell.
## Choose a working prefix
```bash
export PREFIX="/path/to/your/prefix"; mkdir -p $PREFIX
```
## Set the repository
```bash
export REPO="<enter the git repository here>"
```

## Install all necessary dependencies
Ensure you have 
- `cmake` 
- ROOT 
- the Boost C++ libraries 
- a modern c++ compiler (gcc4.8+ or clang3.5+) 
- `gfortran`

## Build and install pythia6m
Once you have all dependencies installed, you are ready to build pythia6m
```bash
mkdir -p ${PREFIX}/src && cd ${PREFIX}/src
git clone --recurse-submodules ${REPO}
mkdir -p pythia6m/BUILD && cd pythia6m/BUILD
cmake .. -DCMAKE_INSTALL_PREFIX=${PREFIX} -DCMAKE_PREFIX_PATH=${PREFIX}
make -j4
make install
```

## (optional) Add pythia6m to your search path
```bash
export PATH=${PREFIX}/bin:${PATH}
echo "export PATH=\${PREFIX}/bin:\${PATH}" >> ${HOME}/.bash_profile
```

# Running pythia6m
If you added pythia6m to your search path, you can run it by simply typing `pythia6m` in a terminal. 
Else, you can invoke it through `$PREFIX/pythia6m`.

## Command line options
You can view the command line options by running
```bash
pythia6m -h
```
It requires the following arguments:
- `--conf`: a configuration file, see `$PREFIX/share/pythia6m/example-config.json` for an example.
- `--run`: a run number. This is also used as the random seed for this run
- `--events`: the number of events to be generated
- `--out`: the output directory. This is where the output files will be placed

## Output
The generated events are written to a ROOT TTree. The following variables are written to the tree for each of the generated events:
```c++
// event level
int32_t evnum;   // the event index
int32_t evgen;   // the total number of generated events sofar. 
                 // this includes generated events that were somehow cut out
double xsec;     // the total generated cross section
                 // the last stored value is the best estimate of the total
                 // cross section. 
                 // To obtain the cross section for a subset of n events:
                 //     xsec_n = n * xsec / evgen;
int32_t process; // PYTHIA process number
double s;        // s for this event
// particle level
std::vector<int32_t> index;     // The index of this track, equal to the fortran 
                                // index, so the C-index is (index - 1)
std::vector<int32_t> status;    // LUND track status (see pythia manual)
std::vector<int32_t> type;      // LUND particle type
std::vector<int32_t> parent;    // fortran index of the parent, 0 if initial
std::vector<int32_t> daughter1; // fortran index of first daughter, 0 if none
std::vector<int32_t> daughter2; // fortran index of last daughter, 0 if none
std::vector<int32_t> charge;    // charge
std::vector<ROOT::Math::XYZTVector> vertex; // vertex 4-vector
std::vector<ROOT::Math::XYZTVector> mom;    // momentum 4-vector
std::vector<double> mass;       // mass
std::vector<bool> init;         // is this an initial particle?
std::vector<bool> lund;         // is this a final particle produced by PYTHIA?
```

The following additional variables are written when running with an electron beam:
```c++
// event level
double Q2;          // Generated Q2
double nu;          // Generated nu
double x;           // Generated Bjorken-x
double y;           // Generated y
double W2;          // Generated W2
int32_t scat_index; // the C-index of the scattered (NOT THE FORTRAN INDEX)
                    // of the scattered lepton
```

## Advanced 
Instead of running the main executable, you can make your own program in C++ using program/pythia6m.cc as example. 
If you do this, you can use `cmake` to directly link to the pythia librariers.
Alternatively, you could also modify the main program directly (located in the program/pythia6m.cc source file). This latter option is only recommended for quick studies.
