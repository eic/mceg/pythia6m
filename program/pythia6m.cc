#include <Math/Vector4D.h>
#include <TFile.h>
#include <fstream>
#include <memory>
#include <pythia6m/core/configuration.hh>
#include <pythia6m/core/exception.hh>
#include <pythia6m/core/framework.hh>
#include <pythia6m/core/logger.hh>
#include <pythia6m/core/progress_meter.hh>
#include <pythia6m/interface/event.hh>
#include <pythia6m/interface/event_out.hh>
#include <pythia6m/interface/pythia6m_generator.hh>

using namespace pythia6m;

int run_pythia(const configuration& conf, const std::string& output) {

  // get the run info
  const size_t run = conf.get<size_t>("run");
  const size_t events = conf.get<size_t>("events");

  // setup output files:
  //  * root file
  std::shared_ptr<TFile> ofile =
      std::make_shared<TFile>((output + ".root").c_str(), "recreate");
  ep_event_out ev_writer{ofile, "ep_event"};
  //  * lund file (optional)
  std::ofstream olund;
  if (conf.get<bool>("write_lund")) {
    olund.open(output + ".lund");
  }

  // setup pythia6m
  pythia6m_generator pygen{conf, "generator"};

  // generate our events
  progress_meter progress{events};
  for (int iev = 0; iev < events; ++iev) {

    // generate one event
    const auto ev = pygen.generate<ep_event>();

    // add additonal event selection here if needed


    // update progress meter
    progress.update();

    // push event to the output writer
    ev_writer.push(ev);

    // also write lund file if needed
    if (olund) {
      ev.print(olund, [](const particle& p) {
        return true; /*add selection criteria for output lines here */
      });
    }

    // debug output for the first 100 events
    if (global::logger.level() == log_level::DEBUG && iev < 100) {
      PYLIST(2);
    }
    // truncated event record
    else if (global::logger.level() == log_level::JUNK) {
      PYEDIT(1);
      PYLIST(2);
    }
    // full event record
    else if (global::logger.level() == log_level::JUNK2) {
      PYLIST(2);
    }
  }

  // that's all!
  return 0;
}

MAKE_PYTHIA6M_FRAMEWORK(run_pythia)
