#include "framework.hh"

#include <exception>
#include <cstdlib>

#include <pythia6m/core/exception.hh>
#include <pythia6m/core/configuration.hh>
#include <pythia6m/core/logger.hh>
#include <pythia6m/core/stringify.hh>

#include <TSystem.h>

#include <boost/exception/diagnostic_information.hpp>
#include <boost/filesystem.hpp>

// useful aliases
namespace fs = boost::filesystem;
namespace po = boost::program_options;
// utility functions (unnamed namespace)
namespace {
void file_exists(const std::string& file) {
  if (!fs::exists(file)) {
    throw pythia6m::framework_file_error{file};
  }
}
} // ns unnamed

namespace pythia6m {
// =============================================================================
// framework constructor: suppress ROOT signals, parse command line arguments
// and provide for error handling
// =============================================================================
framework::framework(int argc, char* argv[],
                     pythia6m_function_type pythia6m_function) try
    // suppress ROOT signal handler
    : dummy_{root_suppress_signals()}
    // parse the command line
    , args_{parse_arguments(argc, argv)}
    // configuration
    , conf_{get_settings(), "mc"}
    // framework function
    ,pythia6m_function_{pythia6m_function} {
  // talk to the user
  LOG_INFO("pythia6m", "Starting pythia6m framework");
  LOG_INFO("pythia6m", "Configuration file: " + args_["conf"].as<std::string>());

  // output file name
  set_output();

  // write the a copy of the configuration file to the output
  ptree settings;
  conf_.save(settings);
  write_json(output_ + ".json", settings);

  } catch (const framework_help& h) {
    std::cerr << h.what() << std::endl;
    exit(0);
  } catch (const pythia6m::exception& e) {
    LOG_ERROR(e.type(), e.what());
    LOG_ERROR(e.type(), "Run with -h for help.");
    throw e;
  } catch (const boost::exception& e) {
    LOG_CRITICAL("boost::exception", boost::diagnostic_information(e));
    LOG_CRITICAL("boost::exception", "Unhandled boost exception");
    LOG_CRITICAL("boost::exception", "Please contact developer for support.");
    throw pythia6m::exception("Unhandled boost exception", "boost::exception");
  } catch (const std::exception& e) {
    LOG_CRITICAL("std::exception", e.what());
    LOG_CRITICAL("std::exception", "Unhandled standard exception");
    LOG_CRITICAL("std::exception", "Please contact developer for support.");
    throw pythia6m::exception("Unhandled standard exception", "std::exception");
  }

int framework::run() const {
  try{
    LOG_INFO("pythia6m", "Starting event generator...");
    int ret = pythia6m_function_(conf_, output_);
    LOG_INFO("pythia6m", "Finished.");
    return ret;
  } catch (const pythia6m::exception& e) {
    LOG_ERROR(e.type(), e.what());
    LOG_ERROR(e.type(), "Run with -h for help.");
    throw e;
  } catch (const boost::exception& e) {
    LOG_CRITICAL("boost::exception", boost::diagnostic_information(e));
    LOG_CRITICAL("boost::exception", "Unhandled boost exception");
    LOG_CRITICAL("boost::exception", "Please contact developer for support.");
    throw pythia6m::exception("Unhandled boost exception", "boost::exception");
  } catch (const std::exception& e) {
    LOG_CRITICAL("std::exception", e.what());
    LOG_CRITICAL("std::exception", "Unhandled standard exception");
    LOG_CRITICAL("std::exception", "Please contact developer for support.");
    throw pythia6m::exception("Unhandled standard exception", "std::exception");
  }
}
} // ns pythia6m

// =============================================================================
// framework private utility functions 
// =============================================================================
namespace pythia6m {
// =============================================================================
// Implementation: framework::parse_arguments
// Also sets the verbosity level to what was requested
// =============================================================================
po::variables_map framework::parse_arguments(int argc, char* argv[]) const {
  po::variables_map args;
  try {
    po::options_description opts_visible{"Allowed options"};
    opts_visible.add_options()("help,h", "Produce help message")(
        "conf,c", po::value<std::string>()->required()->notifier(file_exists),
        "Configuration JSON file")("run,r", po::value<int>(),
                                   "Run number (also the random seed)")(
        "events,e", po::value<int>(), "Number of events to generate")(
        "verb,v", po::value<unsigned>()->default_value(
                      static_cast<unsigned>(log_level::INFO)),
        "Verbosity level (0 -> 7; 0: silent, 4: default, 5: debug)")(
        "out,o", po::value<std::string>()->required(), "Output file name root");
    po::options_description opts_flags;
    opts_flags.add(opts_visible);
    po::positional_options_description opts_positional;

    po::store(po::command_line_parser(argc, argv)
                  .options(opts_flags)
                  .positional(opts_positional)
                  .run(),
              args);

    // help message requested? (BEFORE notify!)
    if (args.count("help")) {
      throw framework_help{argv[0], opts_visible};
    }
    // do our actual processing
    po::notify(args);
    // set the verbosity level if requested
    if (args.count("verb")) {
      unsigned v{args["verb"].as<unsigned>()};
      LOG_INFO("pythia6m", "Verbosity level: " + std::to_string(v));
      global::logger.set_level(v);
    }
    return args;
  } catch (const po::error& e) {
    throw framework_error{e.what()};
  }
  return args;
}
// =============================================================================
// Implementation: framework::get_settings
// =============================================================================
ptree framework::get_settings() const {
  ptree settings;
  try {
    read_json(args_["conf"].as<std::string>(), settings);
  } catch (const boost::property_tree::ptree_error& e) {
    LOG_ERROR("framework_parse_error", e.what());
    throw framework_parse_error{args_["conf"].as<std::string>()};
  }
  return settings;
}
// =============================================================================
// Implementation: framework::set_output
// =============================================================================
void framework::set_output() {
  // get our run info and number of events to be generated
  const int run = get_option<int>("run");
  const int events = get_option<int>("events");

  output_ = args_["out"].as<std::string>();
  if (output_.back() != '/') {
    output_ += '/';
  }

  // what MC program are we running?
  output_ += conf_.get<std::string>("type");

  // add optional tag
  auto tag = conf_.get_optional<std::string>("tag");
  if (tag && tag->size()) {
    output_ += "." + *tag;
  }
  // add the run info and number of generated events
  char info[1024];
  sprintf(info, ".run%05i-%i", run, events);
  output_ += info;
  LOG_INFO("pythia6m", "Output files will be written to: " + output_ + ".*");
}

// =============================================================================
// Implementation: framework::root_suppress_signals
// Suppress the ROOT signal handlers, as they can cause undefined behavior and
// interfere with debugging
// =============================================================================
int framework::root_suppress_signals() const {
  gSystem->ResetSignal(kSigChild);
  gSystem->ResetSignal(kSigBus);
  gSystem->ResetSignal(kSigSegmentationViolation);
  gSystem->ResetSignal(kSigIllegalInstruction);
  gSystem->ResetSignal(kSigSystem);
  gSystem->ResetSignal(kSigPipe);
  gSystem->ResetSignal(kSigAlarm);
  gSystem->ResetSignal(kSigUrgent);
  gSystem->ResetSignal(kSigFloatingException);
  gSystem->ResetSignal(kSigWindowChanged);
  return 0;
}
} // ns pythia6m

// =============================================================================
// Implementation: Exceptions
// =============================================================================
namespace pythia6m {
framework_file_error::framework_file_error(const std::string& file)
    : framework_error{"No such file or directory: " + file,
                      "framework_file_error"} {}
framework_parse_error::framework_parse_error(const std::string& file)
    : framework_error{"Failed to parse: " + file, "framework_parse_error"} {}
framework_help::framework_help(const std::string& program,
                               const po::options_description& opts)
    : framework_error{message(program, opts), "help"} {}
std::string framework_help::message(const std::string& program,
                                    const po::options_description& opts) const {
  std::stringstream ss;
  ss << "\nUsage: " << program << " [options]\n" << opts
     << "\n";
  return ss.str();
}
} // ns pythia6m
