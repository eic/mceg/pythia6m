#ifndef PYTHIA6M_CORE_EVENT_LOADED
#define PYTHIA6M_CORE_EVENT_LOADED

#include <TLorentzVector.h>
#include <cmath>
#include <functional>
#include <ostream>
#include <pythia6m/pythia/pythia6.h>
#include <vector>
#include <cstring>

// pythia 6 particle data, created from the pythia6 common blocks
namespace pythia6m {

// single particle info
struct particle {
  int32_t index;
  int32_t status;
  int32_t type;
  int32_t parent;
  int32_t daughter1; // this is the first daughter, or zero
  int32_t daughter2; // this is the last daughter, or zero
  int32_t charge;
  TLorentzVector vertex;
  TLorentzVector mom;
  double mass;
  double z;
  double Phperp;
  double t;

  particle() {}
  particle(const int32_t idx)
      : index{idx}
      , status{glb_pyJets.K[0][idx]}
      , type{glb_pyJets.K[1][idx]}
      , parent{glb_pyJets.K[2][idx]}
      , daughter1{glb_pyJets.K[3][idx]}
      , daughter1{glb_pyJets.K[4][idx]}
      , charge{PYCHGE(type) / 3}
      , vertex{glb_pyJets.V[0][idx] / 10, glb_pyJets.V[1][idx] / 10,
               glb_pyJets.V[2][idx] / 10, glb_pyJets.V[3][idx] / 10}
      , mom{glb_pyJets.P[0][idx], glb_pyJets.P[1][idx], glb_pyJets.P[2][idx],
            glb_pyJets.P[3][idx]}
      , mass{glb_pyJets.P[4][idx]} {}

  // initial state particle?
  constexpr bool is_init() const { return status == 21; }
  // lund (final state) particle?
  constexpr bool is_lund() const {
    return status >= 1 && status <= 10 && status != 2 && status != 3;
  }
  // virtual particle?
  constexpr bool is_virt() const { return !is_init() && !is_lund(); }

  // print a LUND particle record for this particle
  void print(std::ostream& os) {
    char buf[1024];
    snprintf(
        buf, 1024, "%4i %4i %4i %4i %4i %4i %12.6e %12.6e %12.6e %12.6e %12.6e "
                   "%12.6e %12.6e %12.6e\n",
        index, charge, 1 /* active */, type, parent, daughter1, mom.Px(),
        mom.Py(), mom.Pz(), mom.E(), mass, vertex.X(), vertex.Y(), vertex.Z());
    os << buf;
  }
};

// main event class, mainly a wrapper around a vector<particle>
struct event {
  int32_t evnum;
  int32_t evgen;
  double xsec;
  int32_t process;
  std::vector<particle> particles;

  event()
      : evnum{glb_event.evNum}
      , evgen{glb_event.nEvGen}
      , xsec{glb_pyPars.pari[0]}
      , process{glb_event.process} {
    for (int idx = 0; idx < glb_pyJets.n; ++idx) {
      particles.push_back(idx);
    }
  }

  constexpr const particle& beam() const {
    return particles[0];
  }
  constexpr const particle& target() const {
    return particles[1];
  }

  // print a LUND event record for all final state particles
  void print(std::ostream& os(),
             std::function<bool(const particle&)> selector) const {
    // check what particles we want to write
    std::vector<int> selected; // indices of the selected particles
    for (const auto& part : particles) {
      if (part.is_lund() && selector(part)) {
        selected.push_back(part.idx);
      }
    }
    // output buffer
    char buf[1024];
    // write the header (only number of events needed)
    snprintf(buf, 1024,
             "%4i %4i %4i %12.6e %12.6e %12.6e %12.6e %12.6e %12.6e %12.6e\n",
             selected.size(), 0, 0, 0, 0, 0, 0, 0, 0, 0);
    os << buf;
    // write the selected particle lines
    for (int i : selected) {
      particles[i].print(os);
    }
  }
};

// e+p leptoproduction 
struct ep_event : event {
  TLorentzVector q;
  double s;
  double Q2;
  double nu;
  double x;
  double y;
  double W2;

  ep_event() : event(),
  q {beam.mom-particles[glb_event.scatIdx].mom}
  
  
  {}

  constexpr const particle& scat() const {
    return particles[glb_event.scatIdx];
  }

};
struct ep_event {
  TLorentzVector beam;
  TLorentzVector target;
  TLorentzVector scat;
  TLorentzVector q;

// gamma+p photoproduction
struct photo_event



};


#endif
