#ifndef PYTHIA6M_FMOTION_LOADED
#define PYTHIA6M_FMOTION_LOADED

#ifdef __cplusplus
extern "C" {
#endif

void initfmtable_(int*, int*, float*, int*);
#define INIT_FM(z, a, fmco, fmd) initfmtable_(&z, &a, &fmco, &fmd)

#ifdef __cplusplus
}
#endif

#endif
