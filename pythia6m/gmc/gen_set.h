#ifndef PYTHIA6M_GMC_GEN_SET_LOADED
#define PYTHIA6M_GMC_GEN_SET_LOADED

#ifdef __cplusplus
extern "C" {
#endif

struct GeneratorSettings {
  int PhotoType;    // user control switch (MSEL, 9.2)
  int DecayPiK;     // Have LUND decay pi+-, K+- and K0_L
  int DecayLamKs;   // Have LUND decay Lambda, Sigma, Xi, Omega, K0_S
  int radgenGenLUT; // generate LUT for pol. rad. corr.
  int radgenUseLUT; // use LUT for pol. rad. corr.
  int enableFM;     // activate or not the FM
  int FMNBins;      // Number of bin for FM discretization
  float FMCutOff;   // Max FM (in GeV)
  char PyModel[4];  // l/N (DIS) or gamma/l pN (GVMD,REAL,DIS,RAD)
  char GenRad[4];   // radiative effects (NO,POL)
  char FStruct[4];  // structure function parameterization
  char R[4];        // param. of R=sL/sT (1990, 1998; or 0 for R=0)
};
extern struct GeneratorSettings common_gen_set_;
#define glb_gen common_gen_set_

#ifdef __cplusplus
}
#endif

#endif
