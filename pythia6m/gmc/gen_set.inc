      common /common_gen_set/
     +  genSet_PhotoType,
     +  genSet_DecayPiK,
     +  genSet_DecayLamKs,
     +  genSet_GenLUT,
     +  genSet_UseLUT,
     +  genSet_enableFM,
     +  genSet_FMNBins,
     +  genSet_FMCutOff,
     +  genSet_PyModel,
     +  genSet_GenRad,
     +  genSet_FStruct,
     +  genSet_R
      save /common_gen_set/

      integer
     +  genSet_PhotoType,
     +  genSet_fastMC,
     +  genSet_enableFM,
     +  genSet_FMNBins

      real*4
     +  genSet_FMCutOff

      logical
     +  genSet_DecayPiK,
     +  genSet_DecayLamKs,
     +  genSet_GenLUT,
     +  genSet_UseLUT

      character*4
     +  genSet_PyModel,
     +  genSet_GenRad,
     +  genSet_FStruct,
     +  genSet_R
