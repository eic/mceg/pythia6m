#ifndef PYTHIA6M_GMC_LOADED
#define PYTHIA6M_GMC_LOADED

#ifdef __cplusplus
extern "C" {
#endif
    void gmc_init_(int* ok);
#define GMC_INIT(ok) gmc_init_(&ok)

    void gmc_event_(int* evgen);
#define GMC_EVENT(evgen) gmc_event_(&evgen)

    void gmc_runstat_();
#define GMC_RUNSTAT() gmc_runstat_();

#ifdef __cplusplus
}
#endif

#endif
