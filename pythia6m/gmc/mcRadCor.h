#ifndef PYTHIA6M_GMC_RADCOR_LOADED
#define PYTHIA6M_GMC_RADCOR_LOADED

#ifdef __cplusplus
extern "C" {
#endif
    
    struct RadCorData {
        int mcRadCor;
        int ID;
        char cType[4];
        float xTrue;
        float yTrue;
        float nuTrue;
        float Q2True;
        float W2True;
        float thetaBrems;
        float phiBrems;
        float sigRad;
        float sigCor;
        float sigCorErr;
        float tailIne;
        float tailEla;
        float tailCoh;
        float vacuum;
        float vertex;
        float small;
        float redFac;
        float EBrems;
        float pxTrue;           // true lepton variables
        float pyTrue;           //
        float pzTrue;           //
        float ETrue;            //
        int mcRadCor_9999;
    };
    extern RadCorData mcRadCor_;
#define glb_rc mcRadCor_

#ifdef __cplusplus
}
#endif


#endif
