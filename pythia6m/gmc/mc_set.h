#ifndef PYTHIA6M_GMC_MC_SET_LOADED
#define PYTHIA6M_GMC_MC_SET_LOADED

#ifdef __cplusplus
extern "C" {
#endif

struct MCSettings {
  int RunNo;             // run number
  int NEvents;           // number of events to generate
  int TarA;              // target atomic number
  int TarZ;              // target charge
  float EneBeam;         // beam energy [GeV]
  float PBValue;         // Beam Polarization [-1,1]
  float PTValue;         // Target Polarization [-1,1]
  float Q2Min;           // min Q2
  float Q2Max;           // max Q2
  float xMin;            // min x
  float xMax;            // max x
  float yMin;            // min y
  float yMax;            // max y
  float EMin;            // min E for a BS beam
  float EMax;            // max E for a BS beam
  float RL;              // radiation length (for a photon beam)
  char BeamParType[4];   // beam particle type
  char PBeam[4];         // Beam polarization type (L)
  char PTarget[4];       // target polarization type (T,L,DT)
  char TargetNucleon[1]; // target nucleon (used in tandem with A/Z)
                         // to properly simulate nuclear target
};
extern struct MCSettings common_mc_set_;
#define glb_mc common_mc_set_

#ifdef __cplusplus
}
#endif

#endif
