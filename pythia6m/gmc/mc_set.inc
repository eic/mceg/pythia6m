      common /common_mc_set/
     +  mcSet_RunNo,
     +  mcSet_NEvents,
     +  mcSet_TarA,
     +  mcSet_TarZ,
     +  mcSet_EneBeam,
     +  mcSet_PBValue,
     +  mcSet_PTValue,
     +  mcSet_Q2Min,
     +  mcSet_Q2Max,
     +  mcSet_XMin,
     +  mcSet_XMax,
     +  mcSet_YMin,
     +  mcSet_YMax,
     +  mcSet_EMin,
     +  mcSet_EMax,
     +  mcSet_RL,
     +  mcSet_BeamParType,
     +  mcSet_PBeam,
     +  mcSet_PTarget,
     +  mcSet_TargetNucleon
      save /common_mc_set/

      integer
     +  mcSet_RunNo,
     +  mcSet_NEvents,
     +  mcSet_TarA,
     +  mcSet_TarZ

      real
     +  mcSet_EneBeam,
     +  mcSet_PBValue,
     +  mcSet_PTValue,
     +  mcSet_Q2Min,
     +  mcSet_Q2Max,
     +  mcSet_XMin,
     +  mcSet_XMax,
     +  mcSet_YMin,
     +  mcSet_EMax,
     +  mcSet_EMin,
     +  mcSet_YMax,
     +  mcSet_RL

      character*4
     +  mcSet_BeamParType,
     +  mcSet_PBeam,
     +  mcSet_PTarget

      character*1
     +  mcSet_TargetNucleon
