#ifndef PYTHIA6M_GMC_EVENT_LOADED
#define PYTHIA6M_GMC_EVENT_LOADED

#ifdef __cplusplus
extern "C" {
#endif
    
    struct EventData {
        // constants
        int beamLType;  // LUND particle code for beam
        // counters
        int evNum;      // current event number
        int nEvGen;     // cumulative number for current event
        int nEvGenPrev; // cummulative number for previous event
        int nErr;       // number of errors
        // event generation
        int process;    // PYTHIA process number (MSTI(1))
        int scatIdx;    // PYTHIA index of the scattered lepton
        float weight;   // event weight
        float Q2Gen;    // vertex kinematics
        float nuGen;    //
        float xGen;     //
        float yGen;     //
        float W2Gen;    //
        float thetaGen; // scattered lepton
        float phiGen;   //
        float EGen;     //
        float pGen;     //
        float pxGen;    // and its 3-vector
        float pyGen;    //
        float pzGen;    //
        float vxGen;    // vertex position
        float vyGen;    //
        float vzGen;    //
        // timing
        float timePrev; // time before current event
        float timeNew;  // time after current event
        float timeMax;  // maximum event generation time
        float timeTot;  // total time spent on event generation
    };
    extern struct EventData mcevnt_;
#define glb_event mcevnt_

#ifdef __cplusplus
}
#endif


#endif
