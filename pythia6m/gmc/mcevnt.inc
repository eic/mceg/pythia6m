! Monte Carlo output logical unit

	integer MCLOUT
	parameter (MCLOUT = 6)

! Useful items that don't change on an event-by-event basis
! (they are initialized by the INIT command)

	integer beamiltype      ! LUND particle code for beam

! Counters

	integer mcievent,		! current event number
     +		mcievgen,		! cumulative ievgen OF current event
     +		mcoldievgen,		! cumulative ievgen BEFORE current ev
     +		mcnerror,		! Monte Carlo errors
     +      mcprocess,      ! PYTHIA process number
     +      mciscat         ! PYTHIA index of the scattered lepton

! Event generation 

	real	weight,					! event weight
     +		genq2, gennu, genx, geny, genw2,	! vertex kinematics
     +		genthe, genphi, geneprim, genpprim,	! scattered lepton
     +		genpx, genpy, genpz,			! scat lepton 3-vector
     +		genvx, genvy, genvz			! vertex position

! Timing

	real	timold,		! time before current event
     +		timnew,		! time after current event
     +		timmax,		! maximum time per event we've seen so far
     +		timtot		! total integrated time spent on events


! And stuff all that in a common block

	common /mcevnt/ 
     +      beamiltype,
     +		mcievent, mcievgen, mcoldievgen, 
     +		mcnerror,
     +      mcprocess, mciscat,
     +		weight,
     +		genq2, gennu, genx, geny, genw2, 
     +		genthe, genphi, geneprim, genpprim,
     +		genpx, genpy, genpz,
     +		genvx, genvy, genvz,
     +		timold, timnew, timmax, timtot
