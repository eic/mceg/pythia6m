#ifndef PYTHIA6M_GMC_RANDOM_LOADED
#define PYTHIA6M_GMC_RANDOM_LOADED

#ifdef __cplusplus
#   include<cstring>
#else
#   include<string.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

//-----------------------------------------------------------------
// The point of this set of routines is to replace all potentially
// used random number generators with functions and subroutines
// that utilize a common seed sequence. In this case:
//
//		the CERNLIB RANLUX series
//
// MC programmers should now always use:
//	RBDMQ	to initialize or obtain status
//	RLU	to get a single 0:1 random number
//	DRLU	to get a single 0:1 random number in double precission
//	NRAN	to get a vector of 0:1 random numbers
//	RANNOR	to get 2 Gaussian random numbers
//
// Documentation on RANLUX can be found here:
//     http://wwwinfo.cern.ch/asdoc/shortwrupsdir/v115/top.html 
//-----------------------------------------------------------------
// Initialization and status retrieval routine for random number sequence
//
//	chopt = ' '	reset sequence NSEQ to the beginning (seeds 0,0)
//	        'S'	set seeds for sequence NSEQ to given values
//	        'G'	get the current seeds for the current sequence
//
//	Note1:	If ISEQ.le.0, the current (last used) sequence is used.
//-----------------------------------------------------------------
    void rndmq_(int* nseed1, int* nseed2, int* nseq,
                const char* chopt, int len_chopt);
#define RNDMQ(nseed1, nseed2, nseq, chopt) \
    rndmq_(&nseed1, &nseed2, &nseq, chopt, strlen(chopt))

    // Replace the obsolete CERNLIB RNDM functions
    float rndm_(float* dummy);
    int irndm_(float* dummy);
#define RNDM(dummy) rndm_(&dummy)
#define IRNDM(dummy) irndm_(&dummy)

    // Replace the obsolete CERNLIB NRAN subroutine
    void nran_(float* array, int* length);
#define NRAN(array, length) nran_(array, &length)

    // Replace the obsolete CERNLIB RANNOR subroutine
    void rannor_(float* a, float* b);
#define RANNOR(a, b) rannor_(&a, &b)

    // Replace the obsolete CERNLIB RNORML subroutine
    void rnorml_(float* array, int* length);
#define RNORML(array, length) rnorml_(array, &length)

    // Replace the F77 RANF
    float ranf_(float* dummy);
#define RANF(dummy) ranf_(&dummy)

    // Replace the JETSET random number generator
    float rlu_(int* dummy);
    double drlu_(int* dummy);
#define RLU(dummy) rlu_(&dummy);
#define DRLU(dummy) drlu_(&dummy);

    // Replace the DIVONNE random number generator
    void ranums_(float* array, int* length);
#define RANUMS(array, length) ranums_(array, &length)

#ifdef __cplusplus
}
#endif

#endif
