#include "event.hh"
#include <cstring>
#include <iostream>
#include <pythia6m/gmc/gen_set.h>
#include <pythia6m/gmc/gmc.h>
#include <pythia6m/gmc/mc_set.h>
#include <pythia6m/gmc/mcevnt.h>
#include <pythia6m/pythia6/pythia6.h>

namespace pythia6m {

particle::particle(const int32_t idx)
    : index{idx} //
    , status{glb_pyJets.K[0][idx]}
    , type{glb_pyJets.K[1][idx]}
    , parent{glb_pyJets.K[2][idx]}
    , daughter1{glb_pyJets.K[3][idx]}
    , daughter2{glb_pyJets.K[4][idx]}
    , charge{PYCHGE(type) / 3}
    , vertex{glb_pyJets.V[0][idx] / 10, glb_pyJets.V[1][idx] / 10,
             glb_pyJets.V[2][idx] / 10, glb_pyJets.V[3][idx] / 10}
    , mom{glb_pyJets.P[0][idx], glb_pyJets.P[1][idx], glb_pyJets.P[2][idx],
          glb_pyJets.P[3][idx]}
    , mass{glb_pyJets.P[4][idx]} {}
void particle::print(std::ostream& os) const {
  // write a LUND particle line. index is stored as the Fortran index
  // (consistent with parent/daughter indices)
  char buf[1024];
  snprintf(
      buf, 1024, "%4i %4i %4i %4i %4i %4i %12.6e %12.6e %12.6e %12.6e %12.6e "
                 "%12.6e %12.6e %12.6e\n",
      index + 1, charge, is_lund() /*only LUND particles labeled as active*/,
      type, parent, daughter1, mom.Px(), mom.Py(), mom.Pz(), mom.E(), mass,
      vertex.X(), vertex.Y(), vertex.Z());
  os << buf;
}

event::event()
    : evnum{glb_event.evNum}
    , evgen{glb_event.nEvGen}
    , xsec{glb_pyPars.pari[0]}
    , process{glb_event.process}
    , s{(particles[0].mom + particles[1].mom).M2()} {
  for (int idx = 0; idx < glb_pyJets.n; ++idx) {
    particles.push_back(idx);
  }
}
event::event(event&& rhs)
    : evnum{rhs.evnum}
    , evgen{rhs.evgen}
    , xsec{rhs.xsec}
    , process{rhs.process}
    , s{rhs.s}
    , particles{std::move(rhs.particles)} {}

event& event::operator=(event&& rhs) {
  evnum = rhs.evnum;
  evgen = rhs.evgen;
  xsec = rhs.xsec;
  process = rhs.process;
  s = rhs.s;
  particles = std::move(rhs.particles);
  return *this;
}
void event::print(std::ostream& os,
                  std::function<bool(const particle&)> selector) const {
  // check what particles we want to write
  std::vector<int> selected; // indices of the selected particles
  for (const auto& part : particles) {
    if (part.is_lund() && selector(part)) {
      selected.push_back(part.index);
    }
  }
  // output buffer
  char buf[1024];
  // write the header (only number of events needed)
  snprintf(buf, 1024,
           "%4zu %4i %4i %12.6e %12.6e %12.6e %12.6e %12.6e %12.6e %12.6e\n",
           selected.size(), 0, 0, 0., 0., 0., 0., 0., 0., 0.);
  os << buf;
  // write the selected particle lines
  for (int i : selected) {
    particles[i].print(os);
  }
}

ep_event::ep_event()
    : event()
    , q{beam().mom - particles[glb_event.scatIdx].mom}
    , Q2{glb_event.Q2Gen}
    , nu{glb_event.nuGen}
    , x{glb_event.xGen}
    , y{glb_event.yGen}
    , W2{glb_event.W2Gen}
    , scat_index{glb_event.scatIdx} {}
ep_event::ep_event(ep_event&& rhs)
    : event{std::move(rhs)}
    , q{rhs.q}
    , Q2{rhs.Q2}
    , nu{rhs.nu}
    , x{rhs.x}
    , y{rhs.y}
    , W2{rhs.W2}
    , scat_index{glb_event.scatIdx} {}
ep_event& ep_event::operator=(ep_event&& rhs) {
  q = rhs.q;
  Q2 = rhs.Q2;
  nu = rhs.nu;
  x = rhs.x;
  y = rhs.y;
  W2 = rhs.W2;
  scat_index = rhs.scat_index;
  static_cast<event&>(*this) = std::move(rhs);
  return *this;
}

photo_event::photo_event() : event() {}

photo_event::photo_event(photo_event&& rhs) : event(std::move(rhs)) {}
photo_event& photo_event::operator=(photo_event&& rhs) {
  static_cast<event&>(*this) = std::move(rhs);
  return *this;
}

} // ns pythia6m
