#ifndef PYTHIA6M_CORE_EVENT_LOADED
#define PYTHIA6M_CORE_EVENT_LOADED

#include <Math/Vector4D.h>
#include <cmath>
#include <functional>
#include <ostream>
#include <vector>

// pythia 6 particle data, created from the pythia6 common blocks
namespace pythia6m {

// single particle info
struct particle {
  int32_t index; // Fortran index (starts at 1)
  int32_t status;
  int32_t type;
  int32_t parent;
  int32_t daughter1; // this is the first daughter, or zero
  int32_t daughter2; // this is the last daughter, or zero
  int32_t charge;
  ROOT::Math::XYZTVector vertex;
  ROOT::Math::XYZTVector mom;
  double mass;

  particle() = default;
  
  // create a particle from using the global pythia particle index
  particle(const int32_t idx);

  // initial state particle?
  constexpr bool is_init() const { return status == 21; }
  // lund (final state) particle?
  constexpr bool is_lund() const {
    return status >= 1 && status <= 10 && status != 2 && status != 3;
  }
  // virtual particle?
  constexpr bool is_virt() const { return !is_init() && !is_lund(); }

  // print a LUND particle record for this particle
  void print(std::ostream& os) const;
};

// main event class, mainly a wrapper around a vector<particle>
struct event {
  int32_t evnum;
  int32_t evgen;
  double xsec;
  int32_t process;
  double s;
  std::vector<particle> particles;

  // create event using the pythia global event info
  event();

  // copy constructors
  event(const event&) = default;
  event(event&& rhs);

  // assignment
  event& operator=(const event&) = default;
  event& operator=(event&& rhs);

  constexpr const particle& beam() const {
    return particles[0];
  }
  constexpr const particle& target() const {
    return particles[1];
  }

  // print a LUND event record for all final state particles
  void print(std::ostream& os,
             std::function<bool(const particle&)> selector) const;
};

// e+p leptoproduction 
struct ep_event : event {
  ROOT::Math::XYZTVector q;
  double Q2;
  double nu;
  double x;
  double y;
  double W2;
  int32_t scat_index;

  ep_event();
  ep_event(const ep_event&) = default;
  ep_event(ep_event&& rhs);
  ep_event& operator=(const ep_event&) = default;
  ep_event& operator=(ep_event&& rhs);

  const particle& scat() const { return particles[scat_index]; }
};
// gamma+p photoproduction
struct photo_event : event {

  photo_event();
  photo_event(const photo_event&) = default;
  photo_event(photo_event&& rhs);
  photo_event& operator=(const photo_event&) = default;
  photo_event& operator=(photo_event&& rhs);

  double t(int32_t index) const {
    return (beam().mom - particles[index].mom).M2();
  }
};


} // ns pythia6m

#endif
