#include <Math/Vector4D.h>
#include <TFile.h>
#include <TTree.h>
#include <memory>
#include <pythia6m/core/assert.hh>
#include <pythia6m/core/logger.hh>
#include <pythia6m/interface/event.hh>
#include <string>
#include <vector>

namespace pythia6m {

// store event to ROOT file
class event_out {
public:
  event_out(std::shared_ptr<TFile> f, const std::string& name) : file_{f} {
    LOG_INFO("event_out", "Initializing ROOT output stream");
    tassert(file_, "invalid file pointer");
    file_->cd();
    tree_ = new TTree(name.c_str(), name.c_str());
    tassert(tree_, "Failed to inialize tree " + name);
    create_branches();
  }
  ~event_out() { tree_->AutoSave(); }

  void push(const event& e) {
    clear();
    evnum = e.evnum;
    evgen = e.evgen;
    xsec = e.xsec;
    process = e.process;
    s = e.s;
    for (const auto& part : e.particles) {
      add(part);
    }
    tree_->Fill();
  }

protected:
  void clear() {
    n_part = 0;
    index.clear();
    status.clear();
    type.clear();
    parent.clear();
    daughter1.clear();
    daughter2.clear();
    charge.clear();
    vertex.clear();
    mom.clear();
    mass.clear();
    init.clear();
    lund.clear();
  }
  void add(const particle& part) {
    n_part += 1;
    index.push_back(part.index + 1);
    status.push_back(part.status);
    type.push_back(part.type);
    parent.push_back(part.parent);
    daughter1.push_back(part.daughter1);
    daughter2.push_back(part.daughter2);
    charge.push_back(part.charge);
    vertex.push_back(part.vertex);
    mom.push_back(part.mom);
    mass.push_back(part.mass);
    init.push_back(part.is_init());
    lund.push_back(part.is_lund());
  }

  std::shared_ptr<TFile> file_;
  TTree* tree_;

private:
  void create_branches() {
    tree_->Branch("event", &evnum);
    tree_->Branch("evgen", &evgen);
    tree_->Branch("xsec", &xsec);
    tree_->Branch("process", &process);
    tree_->Branch("s", &s);
    tree_->Branch("n_part", &n_part);
    tree_->Branch("index", &index);
    tree_->Branch("status", &status);
    tree_->Branch("type", &type);
    tree_->Branch("parent", &daughter1);
    tree_->Branch("daughter1", &daughter1);
    tree_->Branch("daughter2", &daughter2);
    tree_->Branch("charge", &charge);
    tree_->Branch("vertex", &vertex);
    tree_->Branch("mom", &mom);
    tree_->Branch("mass", &mass);
    tree_->Branch("init", &init);
    tree_->Branch("lund", &lund);
  }

  // event data
  int32_t evnum;
  int32_t evgen;
  double xsec;
  int32_t process;
  double s;

  // particle data
  int32_t n_part;
  std::vector<int32_t> index;
  std::vector<int32_t> status;
  std::vector<int32_t> type;
  std::vector<int32_t> parent;
  std::vector<int32_t> daughter1; // this is the first daughter, or zero
  std::vector<int32_t> daughter2; // this is the last daughter, or zero
  std::vector<int32_t> charge;
  std::vector<ROOT::Math::XYZTVector> vertex;
  std::vector<ROOT::Math::XYZTVector> mom;
  std::vector<double> mass;
  std::vector<bool> init;
  std::vector<bool> lund;
};

// derived class for ep-event
class ep_event_out : public event_out {
public:
  ep_event_out(std::shared_ptr<TFile> f, const std::string& name)
      : event_out(std::move(f), name) {
    create_branches();
  }

  void push(const ep_event& e) {
    Q2 = e.Q2;
    nu = e.nu;
    x = e.x;
    y = e.y;
    W2 = e.W2;
    scat_index = e.scat_index;
    event_out::push(e);
  }

private:
  void create_branches() {
    tree_->Branch("Q2", &Q2);
    tree_->Branch("nu", &nu);
    tree_->Branch("x", &x);
    tree_->Branch("y", &y);
    tree_->Branch("W2", &W2);
    tree_->Branch("scat_index", &scat_index);
  }

  double Q2;
  double nu;
  double x;
  double y;
  double W2;
  int32_t scat_index;
};

class photo_event_out : public event_out {
public:
  photo_event_out(std::shared_ptr<TFile> f, const std::string& name)
      : event_out(std::move(f), name) {
    create_branches();
  }

  void push(const photo_event& e) { event_out::push(e); }

private:
  void create_branches() {
    ; // nothing here
  }
};

} // ns pythia6m
