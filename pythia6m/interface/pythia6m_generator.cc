#include "pythia6m_generator.hh"


#include <cstring>
#include <pythia6m/core/exception.hh>
#include <pythia6m/core/logger.hh>
#include <pythia6m/fmotion/fmotion.h>
#include <pythia6m/gmc/gen_set.h>
#include <pythia6m/gmc/gmc.h>
#include <pythia6m/gmc/mc_set.h>
#include <pythia6m/gmc/mcevnt.h>
#include <pythia6m/gmc/random.h>
#include <pythia6m/interface/event.hh>
#include <pythia6m/pythia6/pythia6.h>

// globals from GMC
GeneratorSettings glb_gen;
MCSettings glb_mc;
EventData glb_event;

namespace pythia6m {
pythia6m_generator::pythia6m_generator(const configuration& conf, const string_path& path)
    : conf_(conf, path) {
  LOG_INFO("PYTHIA6M", "Initializing the generator");
  // configuration: first GMC, then the generator (depends on e.g. beam settings
  // in GMC)
  conf_gmc();
  conf_generator();
  // run and events (stored in the top level path of the configuration)
  glb_mc.RunNo = conf.get<int>("run");
  glb_mc.NEvents = conf.get<int>("events");
  LOG_INFO("conf", "Will generate " + std::to_string(glb_mc.NEvents) +
                       " events for run " + std::to_string(glb_mc.RunNo));

  // initialization
  init_random();
  init_event();
  init_gmc();
}

pythia6m_generator::~pythia6m_generator() { 
  GMC_RUNSTAT();
  LOG_INFO("PYTHIA6M",
           "TOTAL Generated events: " + std::to_string(glb_event.nEvGen));
  LOG_INFO("PYTHIA6M", "TOTAL PYTHIA Cross Section [ubarn]: " +
                           std::to_string(glb_pyPars.pari[0]));
}

// =============================================================================
// Pythia6m private: configure generator
// =============================================================================
void pythia6m_generator::conf_generator() const {
  LOG_INFO("conf", "Configuring generator");

  // User control switch MSEL (manual section 9.2, also 8.3.1)
  // MSEL=2 additionally enables the elastic and diffactive components of the
  // VMD and GVMD parts
  glb_gen.PhotoType = 2;

  // Let LUND decay some of the semi-stable particles
  glb_gen.DecayPiK = 0;
  glb_gen.DecayLamKs = 1;

  // RADGEN options, currently disabled
  glb_gen.radgenGenLUT = 0;
  glb_gen.radgenUseLUT = 0;
  sprintf(glb_gen.GenRad, "NO\n");

  // reconsider fermi motion when running for A=1 options
  if (glb_mc.TarA == 1) {
    glb_gen.enableFM = 0;
  } else {
    glb_gen.enableFM = 1;
  }
  glb_gen.FMNBins = 1000; // Number of bins for FM discretization
  glb_gen.FMCutOff = 1.5; // Max FM (in GeV)

  // Parameterizations for F2Pythia and R
  // (I believe these are only relevant when running with RADGEN)
  sprintf(glb_gen.FStruct, "F2PY");
  sprintf(glb_gen.R, "1990");

  // PYTHIA model to run with:
  //  * RAD for realistic DIS
  //  * REAL for real photon beam
  if (!strcmp(glb_mc.BeamParType, "GAM")) {
    sprintf(glb_gen.PyModel, "REAL");
  } else {
    sprintf(glb_gen.PyModel, "RAD");
    //sprintf(glb_gen.PyModel, "GVMD");
  }
}
// =============================================================================
// Pythia6m private: configure GMC
// =============================================================================
void pythia6m_generator::conf_gmc() const {
  LOG_INFO("conf", "Configuring MC");

  glb_mc.TarA = conf_.get<int>("target/A");
  glb_mc.TarA = conf_.get<int>("target/Z");
  glb_mc.TargetNucleon[0] = conf_.get<char>("target/nucleon");
  glb_mc.EneBeam = conf_.get<double>("beam/energy");

  // beam particle
  std::string beam = conf_.get<std::string>("beam/particle");
  if (beam != "ELEC" && beam != "POSI" && beam != "GAM") {
    LOG_ERROR("conf_gmc", "Valid beam particles are 'ELEC', 'POSI' or 'GAM'");
    throw conf_.value_error("beam/particle");
  }
  sprintf(glb_mc.BeamParType, "%s", beam.c_str());
  if (beam == "GAM") {
    // real photon setup
    LOG_INFO("pythia6", "Setting up MC for photoproduction");
    glb_mc.Q2Min = 0;
    glb_mc.Q2Max = 100000;
    glb_mc.xMin = 0.;
    glb_mc.xMax = 1.;
    glb_mc.yMin = 0.;
    glb_mc.yMax = 1.;
    glb_mc.EMin = conf_.get<double>("E/min");
    glb_mc.EMax = fmin(conf_.get<double>("E/max"), glb_mc.EneBeam);
    glb_mc.RL = conf_.get<double>("beam/RL");
  } else {
    // electron or positron
    LOG_INFO("pythia6", "Setting up MC for electroproduction");
    glb_mc.Q2Min = conf_.get<double>("Q2/min");
    glb_mc.Q2Max = conf_.get<double>("Q2/max");
    glb_mc.xMin = conf_.get<double>("x/min");
    glb_mc.xMax = conf_.get<double>("x/max");
    glb_mc.yMin = conf_.get<double>("y/min");
    glb_mc.yMax = conf_.get<double>("y/max");
    glb_mc.EMin = 0.;
    glb_mc.EMax = 10000000.;
    glb_mc.RL = 0;
  }

  // hardcoded Beam and target Polarization
  // needed because RADGEN can also run with polarized generators
  // (pythia is unpolarized)
  glb_mc.PBValue = 0;
  glb_mc.PTValue = 0;
  sprintf(glb_mc.PBeam, "L\n");
  sprintf(glb_mc.PTarget, "L\n");
}
// =============================================================================
// Pythia6m private: init event data
// =============================================================================
void pythia6m_generator::init_event() const {
  LOG_INFO("init", "Initializing the event common blocks");
  // beam particle
  if (!strncmp(glb_mc.BeamParType, "POSI", 4)) {
    glb_event.beamLType = -11;
  } else if (!strncmp(glb_mc.BeamParType, "ELEC", 4)) {
    glb_event.beamLType = 11;
  } else if (!strncmp(glb_mc.BeamParType, "GAM", 3)) {
    glb_event.beamLType = 22;
  } else {
    throw exception("Unrecognized beam particle. This should NEVER HAPPEN!");
  }

  // Init FM engine
  if (glb_gen.enableFM) {
    INIT_FM(glb_mc.TarZ, glb_mc.TarA, glb_gen.FMCutOff, glb_gen.FMNBins);
  }

  glb_event.evNum = 0;
  glb_event.nEvGen = 0;
  glb_event.nEvGenPrev = 0;
  glb_event.nErr = 0;

  glb_event.weight = 1.f;
  glb_event.Q2Gen = 0.f;
  glb_event.nuGen = 0.f;
  glb_event.xGen = 0.f;
  glb_event.yGen = 0.f;
  glb_event.W2Gen = 0.f;
  glb_event.thetaGen = 0.f;
  glb_event.phiGen = 0.f;
  glb_event.EGen = 0.f;
  glb_event.pGen = 0.f;
  glb_event.pxGen = 0.f;
  glb_event.pyGen = 0.f;
  glb_event.pzGen = 0.f;
  glb_event.vxGen = 0.f;
  glb_event.vyGen = 0.f;
  glb_event.vzGen = 0.f;
}
// =============================================================================
// Pythia6m private: init GMC
// =============================================================================
void pythia6m_generator::init_gmc() const {
  LOG_INFO("init", "Initializing GMC/PYTHIA");
  int ok;
  GMC_INIT(ok);
  if (!ok) {
    throw exception("Failed to initialized GMC/PYTHIA");
  }
}
// =============================================================================
// Pythia6m private: init RNG
// =============================================================================
void pythia6m_generator::init_random() const {
  LOG_INFO("init",
           "initializing the random generator with seed (run number): " +
               std::to_string(glb_mc.RunNo));
  int dummy1{0};
  int dummy2{0};
  const char* command{" "};
  RNDMQ(dummy1, dummy2, glb_mc.RunNo, command);
}

} // ns pythia6m
