#ifndef PYTHIA6M_CORE_PYTHIA6M_GENERATOR_LOADED
#define PYTHIA6M_CORE_PYTHIA6M_GENERATOR_LOADED

#include <cstring>
#include <pythia6m/core/configuration.hh>
#include <pythia6m/fmotion/fmotion.h>
#include <pythia6m/gmc/gen_set.h>
#include <pythia6m/gmc/gmc.h>
#include <pythia6m/gmc/mc_set.h>
#include <pythia6m/gmc/mcevnt.h>
#include <pythia6m/gmc/random.h>
#include <pythia6m/interface/event.hh>
#include <pythia6m/pythia6/pythia6.h>

namespace pythia6m {

// =============================================================================
// pythia6m_generator MC master class
// =============================================================================
class pythia6m_generator {
public:
  pythia6m_generator(const configuration& conf, const string_path& path);
  ~pythia6m_generator();

  template <class Event> Event generate() const;
  configuration& conf() { return conf_; }
  const configuration& conf() const { return conf_; }

private:
  void conf_generator() const;
  void conf_gmc() const;
  void init_event() const;
  void init_gmc() const;
  void init_random() const;
  // void init_radgen(); // TODO

  void clear_event_kinematics() const;

  configuration conf_;
};

// =============================================================================
// implementation/pythia6m_generator: generate a new good event
// =============================================================================
template <class Event> Event pythia6m_generator::generate() const {
  // start out clean
  clear_event_kinematics();
  int nEvGen{0};

  // generate a good event
  GMC_EVENT(nEvGen);

  // update counts
  glb_event.nEvGen += nEvGen;
  glb_event.evNum += 1;

  // get all event info (created from the globals, event type specified by
  // user)
  Event ev;
  return ev;
}
// =============================================================================
// implementation/pythia6m_generator private: clear_event_kinematics
// =============================================================================
inline void pythia6m_generator::clear_event_kinematics() const {
  glb_event.scatIdx = -1;
  glb_event.Q2Gen = 0.f;
  glb_event.nuGen = 0.f;
  glb_event.xGen = 0.f;
  glb_event.yGen = 0.f;
  glb_event.W2Gen = 0.f;
  glb_event.pxGen = 0.f;
  glb_event.pyGen = 0.f;
  glb_event.pzGen = 0.f;
  glb_event.vxGen = 0.f;
  glb_event.vyGen = 0.f;
  glb_event.vzGen = 0.f;
}

} // ns pythia6m
#endif
