################################################################################
## CMAKE Settings
################################################################################
set (LIBRARY "pythia6m_pythia")
set (TARGETS ${TARGETS} ${LIBRARY} PARENT_SCOPE)

################################################################################
## Sources and install headers
################################################################################
file (GLOB SOURCES "*.F")
file (GLOB HEADERS "*.inc" "*.h")

################################################################################
## Include directories
################################################################################
include_directories("${CMAKE_CURRENT_SOURCE_DIR}")
include_directories("${PROJECT_SOURCE_DIR}/pythia6m/gmc")
include_directories("${PROJECT_SOURCE_DIR}/pythia6m/radgen")

################################################################################
## Compile and Link
################################################################################
add_library(${LIBRARY} STATIC ${SOURCES})
set_target_properties(${LIBRARY} PROPERTIES 
  VERSION ${PYTHIA6M_VERSION} 
  SOVERSION ${PYTHIA6M_SOVERSION}
  PUBLIC_HEADER "${HEADERS}")

################################################################################
## Export and Install
################################################################################
install(TARGETS ${LIBRARY}
  EXPORT ${PROJECT_NAME}-targets
  RUNTIME DESTINATION "${INSTALL_BIN_DIR}" COMPONENT bin
  LIBRARY DESTINATION "${INSTALL_LIB_DIR}" COMPONENT lib
  ARCHIVE DESTINATION "${INSTALL_LIB_DIR}" COMPONENT lib
  PUBLIC_HEADER DESTINATION "${INSTALL_INCLUDE_DIR}/pythia" COMPONENT dev)
