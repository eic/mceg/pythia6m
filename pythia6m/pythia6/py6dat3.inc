C
C Changes:
C   01-06-2002 (LF) - increase MDME array size from (4000,2) -> (8000,2)
C                   - increase BRAT array size from 4000 -> 8000
C
C-----------------------------------------------------------------
 
C...Decay information.
      COMMON/PYDAT3/MDCY(500,3),MDME(8000,2),BRAT(8000),KFDP(8000,5)
      INTEGER MDCY,MDME,KFDP
      DOUBLE PRECISION BRAT
      SAVE/PYDAT3/
