#ifndef PYTHIA6M_PYTHIA6_LOADED
#define PYTHIA6M_PYTHIA6_LOADED

#ifdef __cplusplus
extern "C" {
#endif

// py6pars common block
struct PyParsDef {
  int mstp[200];
  double parp[200];
  int msti[200];
  double pari[200];
};
extern PyParsDef pypars_;
#define glb_pyPars pypars_

// pyjets common block
struct PyJetsDef {
  int n;
  int dummy;
  int K[5][4000];
  double P[5][4000];
  double V[5][4000];
};
extern PyJetsDef pyjets_;
#define glb_pyJets pyjets_

// get particle charge from a KF code
int pychge_(int* kf);
#define PYCHGE(kf) pychge_(&kf)

// print out event info
void pylist_(int* mlist);
#define PYLIST(mlist)                                                          \
  {                                                                            \
    int mlist_ = mlist;                                                        \
    pylist_(&mlist_);                                                          \
  }

// cleanup event info
void pyedit_(int* medit);
#define PYEDIT(medit)                                                          \
  {                                                                            \
    int medit_ = medit;                                                        \
    pyedit_(&medit_);                                                           \
  }

#ifdef __cplusplus
}
#endif

#endif
