#ifndef PYTHIA6M_RADGEN_LOADED
#define PYTHIA6M_RADGEN_LOADED

#ifdef __cplusplus
extern "C" {
#endif
    void radgen_init_(int* useLUT, int* genLUT);
#define RADGEN_INIT(u, g) radgen_init_(&u, &g)

#ifdef __cplusplus
}
#endif

#endif
